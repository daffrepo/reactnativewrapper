require 'json'

package = JSON.parse(File.read(File.join(__dir__, 'package.json')))

Pod::Spec.new do |s|
  s.name           = 'react-native-onfido-sdk'
  s.version        = package['version']
  s.summary        = package['description']
  s.description    = package['description']
  s.license        = package['license']
  s.author         = package['author']
  s.homepage       = package['homepage']
  s.source         = " https://swatidaffodilsw@bitbucket.org/daffrepo/reactnativewrapper.git"

  s.requires_arc   = true
  s.platform       = :ios, '11.0'

  s.preserve_paths = 'LICENSE', 'README.md', 'package.json', 'index.js'
  s.source_files   = 'ios/*.{h,m}'

  # ADD THIS
  s.resource_bundles = {
    'Resources' => ['./*.xcassets']
  }

  s.dependency 'React'
  s.dependency 'Onfido'
end